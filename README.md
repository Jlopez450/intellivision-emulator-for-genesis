The idea was to emulate the Intellivision CPU with a big jump table. I hoped to leverage the YM2612 to play back Intellivision sounds with custom FM instruments. The Genesis VDP could be used to render intellivision sprites and background graphics by translating them to the VDP's format.

I bit of a little too much with this one. I still would like to finish it one day.
