instruction_table:
 dc.w hex00;halt
 dc.w hex01;set double data
 dc.w hex02;enable int
 dc.w hex03;disable int
 dc.w hex04;jump
 dc.w hex05;terminate interrupt
 dc.w hex06;clear carry
 dc.w hex07;set carry
 dc.w hex08;increment
 dc.w hex09
 dc.w hex0a
 dc.w hex0b
 dc.w hex0c
 dc.w hex0d
 dc.w hex0e
 dc.w hex0f;
 dc.w hex10;decrement
 dc.w hex11
 dc.w hex12
 dc.w hex13
 dc.w hex14
 dc.w hex15
 dc.w hex16
 dc.w hex17;
 dc.w hex18;compliment register
 dc.w hex19
 dc.w hex1a
 dc.w hex1b
 dc.w hex1c
 dc.w hex1d
 dc.w hex1e
 dc.w hex1f;
 dc.w hex20;negate
 dc.w hex21
 dc.w hex22
 dc.w hex23
 dc.w hex24
 dc.w hex25
 dc.w hex26
 dc.w hex27;
 dc.w hex28;add carry
 dc.w hex29
 dc.w hex2a
 dc.w hex2b
 dc.w hex2c
 dc.w hex2d
 dc.w hex2e
 dc.w hex2f;
 dc.w hex30;get status word
 dc.w hex31
 dc.w hex32
 dc.w hex33;
 dc.w hex34;nop
 dc.w hex35;nop
 dc.w hex36;software interrupt
 dc.w hex37;software interrupt
 dc.w hex38;return status word
 dc.w hex39
 dc.w hex3a
 dc.w hex3b
 dc.w hex3c
 dc.w hex3d
 dc.w hex3e
 dc.w hex3f;
 dc.w hex40
 dc.w hex41
 dc.w hex42
 dc.w hex43
 dc.w hex44
 dc.w hex45
 dc.w hex46
 dc.w hex47
 dc.w hex48
 dc.w hex49
 dc.w hex4a
 dc.w hex4b
 dc.w hex4c
 dc.w hex4d
 dc.w hex4e
 dc.w hex4f
 dc.w hex50
 dc.w hex51
 dc.w hex52
 dc.w hex53
 dc.w hex54
 dc.w hex55
 dc.w hex56
 dc.w hex57
 dc.w hex58
 dc.w hex59
 dc.w hex5a
 dc.w hex5b
 dc.w hex5c
 dc.w hex5d
 dc.w hex5e
 dc.w hex5f
 dc.w hex60
 dc.w hex61
 dc.w hex62
 dc.w hex63
 dc.w hex64
 dc.w hex65
 dc.w hex66
 dc.w hex67
 dc.w hex68
 dc.w hex69
 dc.w hex6a
 dc.w hex6b
 dc.w hex6c
 dc.w hex6d
 dc.w hex6e
 dc.w hex6f
 dc.w hex70
 dc.w hex71
 dc.w hex72
 dc.w hex73
 dc.w hex74
 dc.w hex75
 dc.w hex76
 dc.w hex77
 dc.w hex78
 dc.w hex79
 dc.w hex7a
 dc.w hex7b
 dc.w hex7c
 dc.w hex7d
 dc.w hex7e
 dc.w hex7f
 dc.w hex80
 dc.w hex81
 dc.w hex82
 dc.w hex83
 dc.w hex84
 dc.w hex85
 dc.w hex86
 dc.w hex87
 dc.w hex88
 dc.w hex89
 dc.w hex8a
 dc.w hex8b
 dc.w hex8c
 dc.w hex8d
 dc.w hex8e
 dc.w hex8f
 dc.w hex90
 dc.w hex91
 dc.w hex92
 dc.w hex93
 dc.w hex94
 dc.w hex95
 dc.w hex96
 dc.w hex97
 dc.w hex98
 dc.w hex99
 dc.w hex9a
 dc.w hex9b
 dc.w hex9c
 dc.w hex9d
 dc.w hex9e
 dc.w hex9f
 dc.w hexa0
 dc.w hexa1
 dc.w hexa2
 dc.w hexa3
 dc.w hexa4
 dc.w hexa5
 dc.w hexa6
 dc.w hexa7
 dc.w hexa8
 dc.w hexa9
 dc.w hexaa
 dc.w hexab
 dc.w hexac
 dc.w hexad
 dc.w hexae
 dc.w hexaf
 dc.w hexb0
 dc.w hexb1
 dc.w hexb2
 dc.w hexb3
 dc.w hexb4
 dc.w hexb5
 dc.w hexb6
 dc.w hexb7
 dc.w hexb8
 dc.w hexb9
 dc.w hexba
 dc.w hexbb
 dc.w hexbc
 dc.w hexbd
 dc.w hexbe
 dc.w hexbf
 dc.w hexc0
 dc.w hexc1
 dc.w hexc2
 dc.w hexc3
 dc.w hexc4
 dc.w hexc5
 dc.w hexc6
 dc.w hexc7
 dc.w hexc8
 dc.w hexc9
 dc.w hexca
 dc.w hexcb
 dc.w hexcc
 dc.w hexcd
 dc.w hexce
 dc.w hexcf
 dc.w hexd0
 dc.w hexd1
 dc.w hexd2
 dc.w hexd3
 dc.w hexd4
 dc.w hexd5
 dc.w hexd6
 dc.w hexd7
 dc.w hexd8
 dc.w hexd9
 dc.w hexda
 dc.w hexdb
 dc.w hexdc
 dc.w hexdd
 dc.w hexde
 dc.w hexdf
 dc.w hexe0
 dc.w hexe1
 dc.w hexe2
 dc.w hexe3
 dc.w hexe4
 dc.w hexe5
 dc.w hexe6
 dc.w hexe7
 dc.w hexe8
 dc.w hexe9
 dc.w hexea
 dc.w hexeb
 dc.w hexec
 dc.w hexed
 dc.w hexee
 dc.w hexef
 dc.w hexf0
 dc.w hexf1
 dc.w hexf2
 dc.w hexf3
 dc.w hexf4
 dc.w hexf5
 dc.w hexf6
 dc.w hexf7
 dc.w hexf8
 dc.w hexf9
 dc.w hexfa
 dc.w hexfb
 dc.w hexfc
 dc.w hexfd
 dc.w hexfe
 dc.w hexff
 dc.w hex100
 dc.w hex101
 dc.w hex102
 dc.w hex103
 dc.w hex104
 dc.w hex105
 dc.w hex106
 dc.w hex107
 dc.w hex108
 dc.w hex109
 dc.w hex10a
 dc.w hex10b
 dc.w hex10c
 dc.w hex10d
 dc.w hex10e
 dc.w hex10f
 dc.w hex110
 dc.w hex111
 dc.w hex112
 dc.w hex113
 dc.w hex114
 dc.w hex115
 dc.w hex116
 dc.w hex117
 dc.w hex118
 dc.w hex119
 dc.w hex11a
 dc.w hex11b
 dc.w hex11c
 dc.w hex11d
 dc.w hex11e
 dc.w hex11f
 dc.w hex120
 dc.w hex121
 dc.w hex122
 dc.w hex123
 dc.w hex124
 dc.w hex125
 dc.w hex126
 dc.w hex127
 dc.w hex128
 dc.w hex129
 dc.w hex12a
 dc.w hex12b
 dc.w hex12c
 dc.w hex12d
 dc.w hex12e
 dc.w hex12f
 dc.w hex130
 dc.w hex131
 dc.w hex132
 dc.w hex133
 dc.w hex134
 dc.w hex135
 dc.w hex136
 dc.w hex137
 dc.w hex138
 dc.w hex139
 dc.w hex13a
 dc.w hex13b
 dc.w hex13c
 dc.w hex13d
 dc.w hex13e
 dc.w hex13f
 dc.w hex140
 dc.w hex141
 dc.w hex142
 dc.w hex143
 dc.w hex144
 dc.w hex145
 dc.w hex146
 dc.w hex147
 dc.w hex148
 dc.w hex149
 dc.w hex14a
 dc.w hex14b
 dc.w hex14c
 dc.w hex14d
 dc.w hex14e
 dc.w hex14f
 dc.w hex150
 dc.w hex151
 dc.w hex152
 dc.w hex153
 dc.w hex154
 dc.w hex155
 dc.w hex156
 dc.w hex157
 dc.w hex158
 dc.w hex159
 dc.w hex15a
 dc.w hex15b
 dc.w hex15c
 dc.w hex15d
 dc.w hex15e
 dc.w hex15f
 dc.w hex160
 dc.w hex161
 dc.w hex162
 dc.w hex163
 dc.w hex164
 dc.w hex165
 dc.w hex166
 dc.w hex167
 dc.w hex168
 dc.w hex169
 dc.w hex16a
 dc.w hex16b
 dc.w hex16c
 dc.w hex16d
 dc.w hex16e
 dc.w hex16f
 dc.w hex170
 dc.w hex171
 dc.w hex172
 dc.w hex173
 dc.w hex174
 dc.w hex175
 dc.w hex176
 dc.w hex177
 dc.w hex178
 dc.w hex179
 dc.w hex17a
 dc.w hex17b
 dc.w hex17c
 dc.w hex17d
 dc.w hex17e
 dc.w hex17f
 dc.w hex180
 dc.w hex181
 dc.w hex182
 dc.w hex183
 dc.w hex184
 dc.w hex185
 dc.w hex186
 dc.w hex187
 dc.w hex188
 dc.w hex189
 dc.w hex18a
 dc.w hex18b
 dc.w hex18c
 dc.w hex18d
 dc.w hex18e
 dc.w hex18f
 dc.w hex190
 dc.w hex191
 dc.w hex192
 dc.w hex193
 dc.w hex194
 dc.w hex195
 dc.w hex196
 dc.w hex197
 dc.w hex198
 dc.w hex199
 dc.w hex19a
 dc.w hex19b
 dc.w hex19c
 dc.w hex19d
 dc.w hex19e
 dc.w hex19f
 dc.w hex1a0
 dc.w hex1a1
 dc.w hex1a2
 dc.w hex1a3
 dc.w hex1a4
 dc.w hex1a5
 dc.w hex1a6
 dc.w hex1a7
 dc.w hex1a8
 dc.w hex1a9
 dc.w hex1aa
 dc.w hex1ab
 dc.w hex1ac
 dc.w hex1ad
 dc.w hex1ae
 dc.w hex1af
 dc.w hex1b0
 dc.w hex1b1
 dc.w hex1b2
 dc.w hex1b3
 dc.w hex1b4
 dc.w hex1b5
 dc.w hex1b6
 dc.w hex1b7
 dc.w hex1b8
 dc.w hex1b9
 dc.w hex1ba
 dc.w hex1bb
 dc.w hex1bc
 dc.w hex1bd
 dc.w hex1be
 dc.w hex1bf
 dc.w hex1c0
 dc.w hex1c1
 dc.w hex1c2
 dc.w hex1c3
 dc.w hex1c4
 dc.w hex1c5
 dc.w hex1c6
 dc.w hex1c7
 dc.w hex1c8
 dc.w hex1c9
 dc.w hex1ca
 dc.w hex1cb
 dc.w hex1cc
 dc.w hex1cd
 dc.w hex1ce
 dc.w hex1cf
 dc.w hex1d0
 dc.w hex1d1
 dc.w hex1d2
 dc.w hex1d3
 dc.w hex1d4
 dc.w hex1d5
 dc.w hex1d6
 dc.w hex1d7
 dc.w hex1d8
 dc.w hex1d9
 dc.w hex1da
 dc.w hex1db
 dc.w hex1dc
 dc.w hex1dd
 dc.w hex1de
 dc.w hex1df
 dc.w hex1e0
 dc.w hex1e1
 dc.w hex1e2
 dc.w hex1e3
 dc.w hex1e4
 dc.w hex1e5
 dc.w hex1e6
 dc.w hex1e7
 dc.w hex1e8
 dc.w hex1e9
 dc.w hex1ea
 dc.w hex1eb
 dc.w hex1ec
 dc.w hex1ed
 dc.w hex1ee
 dc.w hex1ef
 dc.w hex1f0
 dc.w hex1f1
 dc.w hex1f2
 dc.w hex1f3
 dc.w hex1f4
 dc.w hex1f5
 dc.w hex1f6
 dc.w hex1f7
 dc.w hex1f8
 dc.w hex1f9
 dc.w hex1fa
 dc.w hex1fb
 dc.w hex1fc
 dc.w hex1fd
 dc.w hex1fe
 dc.w hex1ff
 dc.w hex200
 dc.w hex201
 dc.w hex202
 dc.w hex203
 dc.w hex204
 dc.w hex205
 dc.w hex206
 dc.w hex207
 dc.w hex208
 dc.w hex209
 dc.w hex20a
 dc.w hex20b
 dc.w hex20c
 dc.w hex20d
 dc.w hex20e
 dc.w hex20f
 dc.w hex210
 dc.w hex211
 dc.w hex212
 dc.w hex213
 dc.w hex214
 dc.w hex215
 dc.w hex216
 dc.w hex217
 dc.w hex218
 dc.w hex219
 dc.w hex21a
 dc.w hex21b
 dc.w hex21c
 dc.w hex21d
 dc.w hex21e
 dc.w hex21f
 dc.w hex220
 dc.w hex221
 dc.w hex222
 dc.w hex223
 dc.w hex224
 dc.w hex225
 dc.w hex226
 dc.w hex227
 dc.w hex228
 dc.w hex229
 dc.w hex22a
 dc.w hex22b
 dc.w hex22c
 dc.w hex22d
 dc.w hex22e
 dc.w hex22f
 dc.w hex230
 dc.w hex231
 dc.w hex232
 dc.w hex233
 dc.w hex234
 dc.w hex235
 dc.w hex236
 dc.w hex237
 dc.w hex238
 dc.w hex239
 dc.w hex23a
 dc.w hex23b
 dc.w hex23c
 dc.w hex23d
 dc.w hex23e
 dc.w hex23f
 dc.w hex240
 dc.w hex241
 dc.w hex242
 dc.w hex243
 dc.w hex244
 dc.w hex245
 dc.w hex246
 dc.w hex247
 dc.w hex248
 dc.w hex249
 dc.w hex24a
 dc.w hex24b
 dc.w hex24c
 dc.w hex24d
 dc.w hex24e
 dc.w hex24f
 dc.w hex250
 dc.w hex251
 dc.w hex252
 dc.w hex253
 dc.w hex254
 dc.w hex255
 dc.w hex256
 dc.w hex257
 dc.w hex258
 dc.w hex259
 dc.w hex25a
 dc.w hex25b
 dc.w hex25c
 dc.w hex25d
 dc.w hex25e
 dc.w hex25f
 dc.w hex260
 dc.w hex261
 dc.w hex262
 dc.w hex263
 dc.w hex264
 dc.w hex265
 dc.w hex266
 dc.w hex267
 dc.w hex268
 dc.w hex269
 dc.w hex26a
 dc.w hex26b
 dc.w hex26c
 dc.w hex26d
 dc.w hex26e
 dc.w hex26f
 dc.w hex270
 dc.w hex271
 dc.w hex272
 dc.w hex273
 dc.w hex274
 dc.w hex275
 dc.w hex276
 dc.w hex277
 dc.w hex278
 dc.w hex279
 dc.w hex27a
 dc.w hex27b
 dc.w hex27c
 dc.w hex27d
 dc.w hex27e
 dc.w hex27f
 dc.w hex280
 dc.w hex281
 dc.w hex282
 dc.w hex283
 dc.w hex284
 dc.w hex285
 dc.w hex286
 dc.w hex287
 dc.w hex288
 dc.w hex289
 dc.w hex28a
 dc.w hex28b
 dc.w hex28c
 dc.w hex28d
 dc.w hex28e
 dc.w hex28f
 dc.w hex290
 dc.w hex291
 dc.w hex292
 dc.w hex293
 dc.w hex294
 dc.w hex295
 dc.w hex296
 dc.w hex297
 dc.w hex298
 dc.w hex299
 dc.w hex29a
 dc.w hex29b
 dc.w hex29c
 dc.w hex29d
 dc.w hex29e
 dc.w hex29f
 dc.w hex2a0
 dc.w hex2a1
 dc.w hex2a2
 dc.w hex2a3
 dc.w hex2a4
 dc.w hex2a5
 dc.w hex2a6
 dc.w hex2a7
 dc.w hex2a8
 dc.w hex2a9
 dc.w hex2aa
 dc.w hex2ab
 dc.w hex2ac
 dc.w hex2ad
 dc.w hex2ae
 dc.w hex2af
 dc.w hex2b0
 dc.w hex2b1
 dc.w hex2b2
 dc.w hex2b3
 dc.w hex2b4
 dc.w hex2b5
 dc.w hex2b6
 dc.w hex2b7
 dc.w hex2b8
 dc.w hex2b9
 dc.w hex2ba
 dc.w hex2bb
 dc.w hex2bc
 dc.w hex2bd
 dc.w hex2be
 dc.w hex2bf
 dc.w hex2c0
 dc.w hex2c1
 dc.w hex2c2
 dc.w hex2c3
 dc.w hex2c4
 dc.w hex2c5
 dc.w hex2c6
 dc.w hex2c7
 dc.w hex2c8
 dc.w hex2c9
 dc.w hex2ca
 dc.w hex2cb
 dc.w hex2cc
 dc.w hex2cd
 dc.w hex2ce
 dc.w hex2cf
 dc.w hex2d0
 dc.w hex2d1
 dc.w hex2d2
 dc.w hex2d3
 dc.w hex2d4
 dc.w hex2d5
 dc.w hex2d6
 dc.w hex2d7
 dc.w hex2d8
 dc.w hex2d9
 dc.w hex2da
 dc.w hex2db
 dc.w hex2dc
 dc.w hex2dd
 dc.w hex2de
 dc.w hex2df
 dc.w hex2e0
 dc.w hex2e1
 dc.w hex2e2
 dc.w hex2e3
 dc.w hex2e4
 dc.w hex2e5
 dc.w hex2e6
 dc.w hex2e7
 dc.w hex2e8
 dc.w hex2e9
 dc.w hex2ea
 dc.w hex2eb
 dc.w hex2ec
 dc.w hex2ed
 dc.w hex2ee
 dc.w hex2ef
 dc.w hex2f0
 dc.w hex2f1
 dc.w hex2f2
 dc.w hex2f3
 dc.w hex2f4
 dc.w hex2f5
 dc.w hex2f6
 dc.w hex2f7
 dc.w hex2f8
 dc.w hex2f9
 dc.w hex2fa
 dc.w hex2fb
 dc.w hex2fc
 dc.w hex2fd
 dc.w hex2fe
 dc.w hex2ff
 dc.w hex300
 dc.w hex301
 dc.w hex302
 dc.w hex303
 dc.w hex304
 dc.w hex305
 dc.w hex306
 dc.w hex307
 dc.w hex308
 dc.w hex309
 dc.w hex30a
 dc.w hex30b
 dc.w hex30c
 dc.w hex30d
 dc.w hex30e
 dc.w hex30f
 dc.w hex310
 dc.w hex311
 dc.w hex312
 dc.w hex313
 dc.w hex314
 dc.w hex315
 dc.w hex316
 dc.w hex317
 dc.w hex318
 dc.w hex319
 dc.w hex31a
 dc.w hex31b
 dc.w hex31c
 dc.w hex31d
 dc.w hex31e
 dc.w hex31f
 dc.w hex320
 dc.w hex321
 dc.w hex322
 dc.w hex323
 dc.w hex324
 dc.w hex325
 dc.w hex326
 dc.w hex327
 dc.w hex328
 dc.w hex329
 dc.w hex32a
 dc.w hex32b
 dc.w hex32c
 dc.w hex32d
 dc.w hex32e
 dc.w hex32f
 dc.w hex330
 dc.w hex331
 dc.w hex332
 dc.w hex333
 dc.w hex334
 dc.w hex335
 dc.w hex336
 dc.w hex337
 dc.w hex338
 dc.w hex339
 dc.w hex33a
 dc.w hex33b
 dc.w hex33c
 dc.w hex33d
 dc.w hex33e
 dc.w hex33f
 dc.w hex340
 dc.w hex341
 dc.w hex342
 dc.w hex343
 dc.w hex344
 dc.w hex345
 dc.w hex346
 dc.w hex347
 dc.w hex348
 dc.w hex349
 dc.w hex34a
 dc.w hex34b
 dc.w hex34c
 dc.w hex34d
 dc.w hex34e
 dc.w hex34f
 dc.w hex350
 dc.w hex351
 dc.w hex352
 dc.w hex353
 dc.w hex354
 dc.w hex355
 dc.w hex356
 dc.w hex357
 dc.w hex358
 dc.w hex359
 dc.w hex35a
 dc.w hex35b
 dc.w hex35c
 dc.w hex35d
 dc.w hex35e
 dc.w hex35f
 dc.w hex360
 dc.w hex361
 dc.w hex362
 dc.w hex363
 dc.w hex364
 dc.w hex365
 dc.w hex366
 dc.w hex367
 dc.w hex368
 dc.w hex369
 dc.w hex36a
 dc.w hex36b
 dc.w hex36c
 dc.w hex36d
 dc.w hex36e
 dc.w hex36f
 dc.w hex370
 dc.w hex371
 dc.w hex372
 dc.w hex373
 dc.w hex374
 dc.w hex375
 dc.w hex376
 dc.w hex377
 dc.w hex378
 dc.w hex379
 dc.w hex37a
 dc.w hex37b
 dc.w hex37c
 dc.w hex37d
 dc.w hex37e
 dc.w hex37f
 dc.w hex380
 dc.w hex381
 dc.w hex382
 dc.w hex383
 dc.w hex384
 dc.w hex385
 dc.w hex386
 dc.w hex387
 dc.w hex388
 dc.w hex389
 dc.w hex38a
 dc.w hex38b
 dc.w hex38c
 dc.w hex38d
 dc.w hex38e
 dc.w hex38f
 dc.w hex390
 dc.w hex391
 dc.w hex392
 dc.w hex393
 dc.w hex394
 dc.w hex395
 dc.w hex396
 dc.w hex397
 dc.w hex398
 dc.w hex399
 dc.w hex39a
 dc.w hex39b
 dc.w hex39c
 dc.w hex39d
 dc.w hex39e
 dc.w hex39f
 dc.w hex3a0
 dc.w hex3a1
 dc.w hex3a2
 dc.w hex3a3
 dc.w hex3a4
 dc.w hex3a5
 dc.w hex3a6
 dc.w hex3a7
 dc.w hex3a8
 dc.w hex3a9
 dc.w hex3aa
 dc.w hex3ab
 dc.w hex3ac
 dc.w hex3ad
 dc.w hex3ae
 dc.w hex3af
 dc.w hex3b0
 dc.w hex3b1
 dc.w hex3b2
 dc.w hex3b3
 dc.w hex3b4
 dc.w hex3b5
 dc.w hex3b6
 dc.w hex3b7
 dc.w hex3b8
 dc.w hex3b9
 dc.w hex3ba
 dc.w hex3bb
 dc.w hex3bc
 dc.w hex3bd
 dc.w hex3be
 dc.w hex3bf
 dc.w hex3c0
 dc.w hex3c1
 dc.w hex3c2
 dc.w hex3c3
 dc.w hex3c4
 dc.w hex3c5
 dc.w hex3c6
 dc.w hex3c7
 dc.w hex3c8
 dc.w hex3c9
 dc.w hex3ca
 dc.w hex3cb
 dc.w hex3cc
 dc.w hex3cd
 dc.w hex3ce
 dc.w hex3cf
 dc.w hex3d0
 dc.w hex3d1
 dc.w hex3d2
 dc.w hex3d3
 dc.w hex3d4
 dc.w hex3d5
 dc.w hex3d6
 dc.w hex3d7
 dc.w hex3d8
 dc.w hex3d9
 dc.w hex3da
 dc.w hex3db
 dc.w hex3dc
 dc.w hex3dd
 dc.w hex3de
 dc.w hex3df
 dc.w hex3e0
 dc.w hex3e1
 dc.w hex3e2
 dc.w hex3e3
 dc.w hex3e4
 dc.w hex3e5
 dc.w hex3e6
 dc.w hex3e7
 dc.w hex3e8
 dc.w hex3e9
 dc.w hex3ea
 dc.w hex3eb
 dc.w hex3ec
 dc.w hex3ed
 dc.w hex3ee
 dc.w hex3ef
 dc.w hex3f0
 dc.w hex3f1
 dc.w hex3f2
 dc.w hex3f3
 dc.w hex3f4
 dc.w hex3f5
 dc.w hex3f6
 dc.w hex3f7
 dc.w hex3f8
 dc.w hex3f9
 dc.w hex3fa
 dc.w hex3fb
 dc.w hex3fc
 dc.w hex3fd
 dc.w hex3fe
 dc.w hex3ff
hex00:
	rts
hex01:
	or.w #%00000100,flags ;flags are SZOCIDXX
	rts
hex02:
	or.w #%00001000,flags
	rts
hex03:
	and.w #%11110111,flags
	rts
hex04:
	rts
hex05:
	rts
hex06:
	and.w #%11101111,flags
	rts
hex07:
	or.w #%00010000,flags
	rts
hex08:
	add.w #$01,reg0
	rts
hex09:
	add.w #$01,reg1
	rts
hex0a:
	add.w #$01,reg2
	rts
hex0b:
	add.w #$01,reg3
	rts
hex0c:
	add.w #$01,reg4
	rts
hex0d:
	add.w #$01,reg5
	rts
hex0e:
	add.w #$01,regsp
	rts
hex0f:
	add.w #$01,regpc
	rts
hex10:
	sub.w #$01,reg0
	rts
hex11:
	sub.w #$01,reg1
	rts
hex12:
	sub.w #$01,reg2
	rts
hex13:
	sub.w #$01,reg3
	rts
hex14:
	sub.w #$01,reg4
	rts
hex15:
	sub.w #$01,reg5
	rts
hex16:
	sub.w #$01,regsp
	rts
hex17:
	sub.w #$01,regpc
	rts
hex18:
	eor.w #$ffff,reg0
	rts
hex19:
	eor.w #$ffff,reg1
	rts
hex1a:
	eor.w #$ffff,reg2
	rts
hex1b:
	eor.w #$ffff,reg3
	rts
hex1c:
	eor.w #$ffff,reg4
	rts
hex1d:
	eor.w #$ffff,reg5
	rts
hex1e:
	eor.w #$ffff,regsp
	rts
hex1f:
	eor.w #$ffff,regpc
	rts
hex20:
	neg reg0
	rts
hex21:
	neg reg1
	rts
hex22:
	neg reg2
	rts
hex23:
	neg reg3
	rts
hex24:
	neg reg4
	rts
hex25:
	neg reg5
	rts
hex26:
	neg regsp
	rts
hex27:
	neg regpc
	rts
hex28:
	move.w flags, d0 ;flags are SZOCIDXX
	andi.w #%00010000,d0
	cmpi.w #$0000,d0 ;ToDo: Do this better
	beq return
	add.w #$01,reg0
	rts
hex29:
	move.w flags, d0
	andi.w #%00010000,d0
	cmpi.w #$0000,d0
	beq return
	add.w #$01,reg1
	rts
hex2a:
	move.w flags, d0
	andi.w #%00010000,d0
	cmpi.w #$0000,d0
	beq return
	add.w #$01,reg2
	rts
hex2b:
	move.w flags, d0
	andi.w #%00010000,d0
	cmpi.w #$0000,d0
	beq return
	add.w #$01,reg3
	rts
hex2c:
	move.w flags, d0
	andi.w #%00010000,d0
	cmpi.w #$0000,d0
	beq return
	add.w #$01,reg4
	rts
hex2d:
	move.w flags, d0
	andi.w #%00010000,d0
	cmpi.w #$0000,d0
	beq return
	add.w #$01,reg5
	rts
hex2e:
	move.w flags, d0
	andi.w #%00010000,d0
	cmpi.w #$0000,d0
	beq return
	add.w #$01,regsp
	rts
hex2f:
	move.w flags, d0
	andi.w #%00010000,d0
	cmpi.w #$0000,d0
	beq return
	add.w #$01,regpc
	rts
hex30:
	move.w flags,d0 ;flags are SZOCIDXX
	andi.w #%11110000,d0
	move.w d0,d1
	lsl.w #$08,d1
	eor.w d1,d0
	move.w d0,reg0
	rts
hex31:
	move.w flags,d0
	andi.w #%11110000,d0
	move.w d0,d1
	lsl.w #$08,d1
	eor.w d1,d0
	move.w d0,reg1
	rts
hex32:
	move.w flags,d0
	andi.w #%11110000,d0
	move.w d0,d1
	lsl.w #$08,d1
	eor.w d1,d0
	move.w d0,reg2
	rts
hex33:
	move.w flags,d0
	andi.w #%11110000,d0
	move.w d0,d1
	lsl.w #$08,d1
	eor.w d1,d0
	move.w d0,reg3
	rts
hex34:
	rts
hex35:
	rts ;nop
hex36:
	rts
hex37:
	rts
hex38:
	move.w reg0,d0
	andi.w #$00F0,d0
	move.w flags,d1
	andi.w #$0006,d1
	or.w d0,d1
	move.w d1,flags
	rts
hex39:
	move.w reg1,d0
	andi.w #$00F0,d0
	move.w flags,d1
	andi.w #$0006,d1
	or.w d0,d1
	move.w d1,flags
	rts
hex3a:
	move.w reg2,d0
	andi.w #$00F0,d0
	move.w flags,d1
	andi.w #$0006,d1
	or.w d0,d1
	move.w d1,flags
	rts
hex3b:
	move.w reg3,d0
	andi.w #$00F0,d0
	move.w flags,d1
	andi.w #$0006,d1
	or.w d0,d1
	move.w d1,flags
	rts
hex3c:
	move.w reg4,d0
	andi.w #$00F0,d0
	move.w flags,d1
	andi.w #$0006,d1
	or.w d0,d1
	move.w d1,flags
	rts
hex3d:
	move.w reg5,d0
	andi.w #$00F0,d0
	move.w flags,d1
	andi.w #$0006,d1
	or.w d0,d1
	move.w d1,flags
	rts
hex3e:
	move.w regsp,d0
	andi.w #$00F0,d0
	move.w flags,d1
	andi.w #$0006,d1
	or.w d0,d1
	move.w d1,flags
	rts
hex3f:
	move.w regpc,d0
	andi.w #$00F0,d0
	move.w flags,d1
	andi.w #$0006,d1
	or.w d0,d1
	move.w d1,flags
	rts
hex40:
	move.w reg0,d0 ;ToDo: deal with sign flag
	move.w d0,d1
	andi.w #$ff00,d0
	andi.w #$00ff,d1
	lsr.w #$08,d0
	lsl.w #$08,d1
	or.w d0,d1
	move.w d1,reg0
	rts
hex41:
	move.w reg1,d0
	move.w d0,d1
	andi.w #$ff00,d0
	andi.w #$00ff,d1
	lsr.w #$08,d0
	lsl.w #$08,d1
	or.w d0,d1
	move.w d1,reg1
	rts
hex42:
	move.w reg2,d0
	move.w d0,d1
	andi.w #$ff00,d0
	andi.w #$00ff,d1
	lsr.w #$08,d0
	lsl.w #$08,d1
	or.w d0,d1
	move.w d1,reg2
	rts
hex43:
	move.w reg3,d0
	move.w d0,d1
	andi.w #$ff00,d0
	andi.w #$00ff,d1
	lsr.w #$08,d0
	lsl.w #$08,d1
	or.w d0,d1
	move.w d1,reg3
	rts
hex44:
	move.w reg0,d0 ;ToDo: also flags
	move.w d0,d1
	lsl.w #$08,d0
	or.w d1,d0
	move.w d0,reg0
	rts
hex45:
	move.w reg1,d0
	move.w d0,d1
	lsl.w #$08,d0
	or.w d1,d0
	move.w d0,reg1
	rts
hex46:
	move.w reg2,d0
	move.w d0,d1
	lsl.w #$08,d0
	or.w d1,d0
	move.w d0,reg2
	rts
hex47:
	move.w reg3,d0
	move.w d0,d1
	lsl.w #$08,d0
	or.w d1,d0
	move.w d0,reg3
	rts
hex48:
	move.w reg0,d0
	lsl.w #$01,d0
	move.w d0,reg0
	rts
hex49:
	move.w reg1,d0
	lsl.w #$01,d0
	move.w d0,reg1
	rts
hex4a:
	move.w reg2,d0
	lsl.w #$01,d0
	move.w d0,reg2
	rts
hex4b:
	move.w reg3,d0
	lsl.w #$01,d0
	move.w d0,reg3
	rts
hex4c:
	move.w reg0,d0
	lsl.w #$02,d0
	move.w d0,reg0
	rts
hex4d:
	move.w reg1,d0
	lsl.w #$02,d0
	move.w d0,reg1
	rts
hex4e:
	move.w reg2,d0
	lsl.w #$02,d0
	move.w d0,reg2
	rts
hex4f:
	move.w reg3,d0
	lsl.w #$02,d0
	move.w d0,reg3
	rts
hex50:
hex51:
hex52:
hex53:
hex54:
hex55:
hex56:
hex57:
hex58:
hex59:
hex5a:
hex5b:
hex5c:
hex5d:
hex5e:
hex5f:
hex60:
	move.w reg0,d0
	lsr.w #$01,d0
	move.w d0,reg0
	rts
hex61:
	move.w reg1,d0
	lsr.w #$01,d0
	move.w d0,reg1
	rts
hex62:
	move.w reg2,d0
	lsr.w #$01,d0
	move.w d0,reg2
	rts
hex63:
	move.w reg3,d0
	lsr.w #$01,d0
	move.w d0,reg3
	rts
hex64:
	move.w reg0,d0
	lsr.w #$02,d0
	move.w d0,reg0
	rts
hex65:
	move.w reg1,d0
	lsr.w #$02,d0
	move.w d0,reg1
	rts
hex66:
	move.w reg2,d0
	lsr.w #$02,d0
	move.w d0,reg2
	rts
hex67:
	move.w reg3,d0
	lsr.w #$02,d0
	move.w d0,reg3
	rts
hex68:
hex69:
hex6a:
hex6b:
hex6c:
hex6d:
hex6e:
hex6f:
hex70:
hex71:
hex72:
hex73:
hex74:
hex75:
hex76:
hex77:
hex78:
hex79:
hex7a:
hex7b:
hex7c:
hex7d:
hex7e:
hex7f:
hex80:
hex81:
hex82:
hex83:
hex84:
hex85:
hex86:
hex87:
hex88:
hex89:
hex8a:
hex8b:
hex8c:
hex8d:
hex8e:
hex8f:
hex90:
hex91:
hex92:
hex93:
hex94:
hex95:
hex96:
hex97:
hex98:
hex99:
hex9a:
hex9b:
hex9c:
hex9d:
hex9e:
hex9f:
hexa0:
hexa1:
hexa2:
hexa3:
hexa4:
hexa5:
hexa6:
hexa7:
hexa8:
hexa9:
hexaa:
hexab:
hexac:
hexad:
hexae:
hexaf:
hexb0:
hexb1:
hexb2:
hexb3:
hexb4:
hexb5:
hexb6:
hexb7:
hexb8:
hexb9:
hexba:
hexbb:
hexbc:
hexbd:
hexbe:
hexbf:
hexc0:
hexc1:
hexc2:
hexc3:
hexc4:
hexc5:
hexc6:
hexc7:
hexc8:
hexc9:
hexca:
hexcb:
hexcc:
hexcd:
hexce:
hexcf:
hexd0:
hexd1:
hexd2:
hexd3:
hexd4:
hexd5:
hexd6:
hexd7:
hexd8:
hexd9:
hexda:
hexdb:
hexdc:
hexdd:
hexde:
hexdf:
hexe0:
hexe1:
hexe2:
hexe3:
hexe4:
hexe5:
hexe6:
hexe7:
hexe8:
hexe9:
hexea:
hexeb:
hexec:
hexed:
hexee:
hexef:
hexf0:
hexf1:
hexf2:
hexf3:
hexf4:
hexf5:
hexf6:
hexf7:
hexf8:
hexf9:
hexfa:
hexfb:
hexfc:
hexfd:
hexfe:
hexff:
hex100:
hex101:
hex102:
hex103:
hex104:
hex105:
hex106:
hex107:
hex108:
hex109:
hex10a:
hex10b:
hex10c:
hex10d:
hex10e:
hex10f:
hex110:
hex111:
hex112:
hex113:
hex114:
hex115:
hex116:
hex117:
hex118:
hex119:
hex11a:
hex11b:
hex11c:
hex11d:
hex11e:
hex11f:
hex120:
hex121:
hex122:
hex123:
hex124:
hex125:
hex126:
hex127:
hex128:
hex129:
hex12a:
hex12b:
hex12c:
hex12d:
hex12e:
hex12f:
hex130:
hex131:
hex132:
hex133:
hex134:
hex135:
hex136:
hex137:
hex138:
hex139:
hex13a:
hex13b:
hex13c:
hex13d:
hex13e:
hex13f:
hex140:
hex141:
hex142:
hex143:
hex144:
hex145:
hex146:
hex147:
hex148:
hex149:
hex14a:
hex14b:
hex14c:
hex14d:
hex14e:
hex14f:
hex150:
hex151:
hex152:
hex153:
hex154:
hex155:
hex156:
hex157:
hex158:
hex159:
hex15a:
hex15b:
hex15c:
hex15d:
hex15e:
hex15f:
hex160:
hex161:
hex162:
hex163:
hex164:
hex165:
hex166:
hex167:
hex168:
hex169:
hex16a:
hex16b:
hex16c:
hex16d:
hex16e:
hex16f:
hex170:
hex171:
hex172:
hex173:
hex174:
hex175:
hex176:
hex177:
hex178:
hex179:
hex17a:
hex17b:
hex17c:
hex17d:
hex17e:
hex17f:
hex180:
hex181:
hex182:
hex183:
hex184:
hex185:
hex186:
hex187:
hex188:
hex189:
hex18a:
hex18b:
hex18c:
hex18d:
hex18e:
hex18f:
hex190:
hex191:
hex192:
hex193:
hex194:
hex195:
hex196:
hex197:
hex198:
hex199:
hex19a:
hex19b:
hex19c:
hex19d:
hex19e:
hex19f:
hex1a0:
hex1a1:
hex1a2:
hex1a3:
hex1a4:
hex1a5:
hex1a6:
hex1a7:
hex1a8:
hex1a9:
hex1aa:
hex1ab:
hex1ac:
hex1ad:
hex1ae:
hex1af:
hex1b0:
hex1b1:
hex1b2:
hex1b3:
hex1b4:
hex1b5:
hex1b6:
hex1b7:
hex1b8:
hex1b9:
hex1ba:
hex1bb:
hex1bc:
hex1bd:
hex1be:
hex1bf:
hex1c0:
hex1c1:
hex1c2:
hex1c3:
hex1c4:
hex1c5:
hex1c6:
hex1c7:
hex1c8:
hex1c9:
hex1ca:
hex1cb:
hex1cc:
hex1cd:
hex1ce:
hex1cf:
hex1d0:
hex1d1:
hex1d2:
hex1d3:
hex1d4:
hex1d5:
hex1d6:
hex1d7:
hex1d8:
hex1d9:
hex1da:
hex1db:
hex1dc:
hex1dd:
hex1de:
hex1df:
hex1e0:
hex1e1:
hex1e2:
hex1e3:
hex1e4:
hex1e5:
hex1e6:
hex1e7:
hex1e8:
hex1e9:
hex1ea:
hex1eb:
hex1ec:
hex1ed:
hex1ee:
hex1ef:
hex1f0:
hex1f1:
hex1f2:
hex1f3:
hex1f4:
hex1f5:
hex1f6:
hex1f7:
hex1f8:
hex1f9:
hex1fa:
hex1fb:
hex1fc:
hex1fd:
hex1fe:
hex1ff:
hex200:
hex201:
hex202:
hex203:
hex204:
hex205:
hex206:
hex207:
hex208:
hex209:
hex20a:
hex20b:
hex20c:
hex20d:
hex20e:
hex20f:
hex210:
hex211:
hex212:
hex213:
hex214:
hex215:
hex216:
hex217:
hex218:
hex219:
hex21a:
hex21b:
hex21c:
hex21d:
hex21e:
hex21f:
hex220:
hex221:
hex222:
hex223:
hex224:
hex225:
hex226:
hex227:
hex228:
hex229:
hex22a:
hex22b:
hex22c:
hex22d:
hex22e:
hex22f:
hex230:
hex231:
hex232:
hex233:
hex234:
hex235:
hex236:
hex237:
hex238:
hex239:
hex23a:
hex23b:
hex23c:
hex23d:
hex23e:
hex23f:
hex240:
hex241:
hex242:
hex243:
hex244:
hex245:
hex246:
hex247:
hex248:
hex249:
hex24a:
hex24b:
hex24c:
hex24d:
hex24e:
hex24f:
hex250:
hex251:
hex252:
hex253:
hex254:
hex255:
hex256:
hex257:
hex258:
hex259:
hex25a:
hex25b:
hex25c:
hex25d:
hex25e:
hex25f:
hex260:
hex261:
hex262:
hex263:
hex264:
hex265:
hex266:
hex267:
hex268:
hex269:
hex26a:
hex26b:
hex26c:
hex26d:
hex26e:
hex26f:
hex270:
hex271:
hex272:
hex273:
hex274:
hex275:
hex276:
hex277:
hex278:
hex279:
hex27a:
hex27b:
hex27c:
hex27d:
hex27e:
hex27f:
hex280:
hex281:
hex282:
hex283:
hex284:
hex285:
hex286:
hex287:
hex288:
hex289:
hex28a:
hex28b:
hex28c:
hex28d:
hex28e:
hex28f:
hex290:
hex291:
hex292:
hex293:
hex294:
hex295:
hex296:
hex297:
hex298:
hex299:
hex29a:
hex29b:
hex29c:
hex29d:
hex29e:
hex29f:
hex2a0:
hex2a1:
hex2a2:
hex2a3:
hex2a4:
hex2a5:
hex2a6:
hex2a7:
hex2a8:
hex2a9:
hex2aa:
hex2ab:
hex2ac:
hex2ad:
hex2ae:
hex2af:
hex2b0:
hex2b1:
hex2b2:
hex2b3:
hex2b4:
hex2b5:
hex2b6:
hex2b7:
hex2b8:
hex2b9:
hex2ba:
hex2bb:
hex2bc:
hex2bd:
hex2be:
hex2bf:
hex2c0:
hex2c1:
hex2c2:
hex2c3:
hex2c4:
hex2c5:
hex2c6:
hex2c7:
hex2c8:
hex2c9:
hex2ca:
hex2cb:
hex2cc:
hex2cd:
hex2ce:
hex2cf:
hex2d0:
hex2d1:
hex2d2:
hex2d3:
hex2d4:
hex2d5:
hex2d6:
hex2d7:
hex2d8:
hex2d9:
hex2da:
hex2db:
hex2dc:
hex2dd:
hex2de:
hex2df:
hex2e0:
hex2e1:
hex2e2:
hex2e3:
hex2e4:
hex2e5:
hex2e6:
hex2e7:
hex2e8:
hex2e9:
hex2ea:
hex2eb:
hex2ec:
hex2ed:
hex2ee:
hex2ef:
hex2f0:
hex2f1:
hex2f2:
hex2f3:
hex2f4:
hex2f5:
hex2f6:
hex2f7:
hex2f8:
hex2f9:
hex2fa:
hex2fb:
hex2fc:
hex2fd:
hex2fe:
hex2ff:
hex300:
hex301:
hex302:
hex303:
hex304:
hex305:
hex306:
hex307:
hex308:
hex309:
hex30a:
hex30b:
hex30c:
hex30d:
hex30e:
hex30f:
hex310:
hex311:
hex312:
hex313:
hex314:
hex315:
hex316:
hex317:
hex318:
hex319:
hex31a:
hex31b:
hex31c:
hex31d:
hex31e:
hex31f:
hex320:
hex321:
hex322:
hex323:
hex324:
hex325:
hex326:
hex327:
hex328:
hex329:
hex32a:
hex32b:
hex32c:
hex32d:
hex32e:
hex32f:
hex330:
hex331:
hex332:
hex333:
hex334:
hex335:
hex336:
hex337:
hex338:
hex339:
hex33a:
hex33b:
hex33c:
hex33d:
hex33e:
hex33f:
hex340:
hex341:
hex342:
hex343:
hex344:
hex345:
hex346:
hex347:
hex348:
hex349:
hex34a:
hex34b:
hex34c:
hex34d:
hex34e:
hex34f:
hex350:
hex351:
hex352:
hex353:
hex354:
hex355:
hex356:
hex357:
hex358:
hex359:
hex35a:
hex35b:
hex35c:
hex35d:
hex35e:
hex35f:
hex360:
hex361:
hex362:
hex363:
hex364:
hex365:
hex366:
hex367:
hex368:
hex369:
hex36a:
hex36b:
hex36c:
hex36d:
hex36e:
hex36f:
hex370:
hex371:
hex372:
hex373:
hex374:
hex375:
hex376:
hex377:
hex378:
hex379:
hex37a:
hex37b:
hex37c:
hex37d:
hex37e:
hex37f:
hex380:
hex381:
hex382:
hex383:
hex384:
hex385:
hex386:
hex387:
hex388:
hex389:
hex38a:
hex38b:
hex38c:
hex38d:
hex38e:
hex38f:
hex390:
hex391:
hex392:
hex393:
hex394:
hex395:
hex396:
hex397:
hex398:
hex399:
hex39a:
hex39b:
hex39c:
hex39d:
hex39e:
hex39f:
hex3a0:
hex3a1:
hex3a2:
hex3a3:
hex3a4:
hex3a5:
hex3a6:
hex3a7:
hex3a8:
hex3a9:
hex3aa:
hex3ab:
hex3ac:
hex3ad:
hex3ae:
hex3af:
hex3b0:
hex3b1:
hex3b2:
hex3b3:
hex3b4:
hex3b5:
hex3b6:
hex3b7:
hex3b8:
hex3b9:
hex3ba:
hex3bb:
hex3bc:
hex3bd:
hex3be:
hex3bf:
hex3c0:
hex3c1:
hex3c2:
hex3c3:
hex3c4:
hex3c5:
hex3c6:
hex3c7:
hex3c8:
hex3c9:
hex3ca:
hex3cb:
hex3cc:
hex3cd:
hex3ce:
hex3cf:
hex3d0:
hex3d1:
hex3d2:
hex3d3:
hex3d4:
hex3d5:
hex3d6:
hex3d7:
hex3d8:
hex3d9:
hex3da:
hex3db:
hex3dc:
hex3dd:
hex3de:
hex3df:
hex3e0:
hex3e1:
hex3e2:
hex3e3:
hex3e4:
hex3e5:
hex3e6:
hex3e7:
hex3e8:
hex3e9:
hex3ea:
hex3eb:
hex3ec:
hex3ed:
hex3ee:
hex3ef:
hex3f0:;XOR immediate
hex3f1:
hex3f2:
hex3f3:
hex3f4:
hex3f5:
hex3f6:
hex3f7:
hex3f8:
hex3f9:
hex3fa:
hex3fb:
hex3fc:
hex3fd:
hex3fe:
hex3ff:	;
	rts	