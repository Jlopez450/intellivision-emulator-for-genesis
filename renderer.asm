render:
	move.l #$628e0000,(a3)
	move.w reg0, d2
	bsr writeword
	
	move.l #$630e0000,(a3)
	move.w reg1, d2
	bsr writeword
	
	move.l #$638e0000,(a3)
	move.w reg2, d2
	bsr writeword
	
	move.l #$640e0000,(a3)
	move.w reg3, d2
	bsr writeword
	
	move.l #$648e0000,(a3)
	move.w reg4, d2
	bsr writeword
	
	move.l #$650e0000,(a3)
	move.w reg5, d2
	bsr writeword
	
	move.l #$660e0000,(a3)
	move.w regsp, d2
	bsr writeword
	
	move.l #$668e0000,(a3)
	move.w regpc, d2
	bsr writeword
	
	move.l #$67920000,(a3)
	move.w flags, d2
	bsr writeword
	
	move.l #$68a00000,(a3)
	move.w currentbyte, d2
	bsr writeword
	
	move.l #$69200000,(a3)
	move.w lastbyte, d2
	bsr writeword
	rts
writeword:
	    move.w d2,d5
		andi.w #$f000,d5
		lsr #$8,d5
		lsr #$4,d5
		andi.w #$00ff, d5
    	add.w #$0030,d5
		move.w d5,(a4)
writetrip:
	    move.w d2,d5
		andi.w #$0f00,d5
		lsr #$8,d5
		andi.w #$00ff, d5
    	add.w #$0030,d5
		move.w d5,(a4)
writebyte:  ;not to be confused with litebrite				
	    move.w d2,d5
		andi.w #$00f0,d5
		lsr #$4,d5
    	add.w #$0030,d5
		move.w d5,(a4)
	    move.w d2,d5
		andi.w #$000f,d5
    	add.w #$0030,d5	
		move.w d5,(a4)		
		rts