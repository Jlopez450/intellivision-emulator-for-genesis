render:
	bsr renderA
	bsr renderX
	bsr rendery
	bsr renderpc
	bsr rendersp
	bsr renderbyte
	bsr renderlastbyte
	bsr renderrealPC
	rts
	
renderA:
	move.l #$0000228e,d0 ;reg a display VRAM position
	bsr calc_vram
	move.l d0,(a3)
	clr d7
	swap d7
	clr d7
	move.b regA,d7
	andi.b #$F0,d7
	lsr.w #$04,d7
	add.w #$30,d7
	move.w d7,(a4)	
	clr d7
	swap d7
	clr d7
	move.b regA,d7
	andi.b #$0F,d7
	add.w #$30,d7
	move.w d7,(a4)		
	rts
	
renderX:
	move.l #$0000238e,d0 ;reg 3 display VRAM position
	bsr calc_vram
	move.l d0,(a3)
	clr d7
	swap d7
	clr d7
	move.w regx,d7
	andi.w #$F000,d7
	lsr #$06,d7
	lsr #$06,d7
	add.w #$30,d7
	move.w d7,(a4)
	move.w regX,d7
	andi.w #$0f00,d7
	lsr #$04,d7
	lsr #$04,d7
	add.w #$30,d7
	move.w d7,(a4)
	move.w regX,d7
	andi.w #$00f0,d7
	lsr #$04,d7
	add.w #$30,d7
	move.w d7,(a4)	
	move.w regX,d7
	andi.w #$000f,d7
	add.w #$30,d7
	move.w d7,(a4)	
	rts
	
rendery:
	move.l #$0000248e,d0 ;reg 3 display VRAM position
	bsr calc_vram
	move.l d0,(a3)
	clr d7
	swap d7
	clr d7
	move.w regy,d7
	andi.w #$F000,d7
	lsr #$06,d7
	lsr #$06,d7
	add.w #$30,d7
	move.w d7,(a4)
	move.w regy,d7
	andi.w #$0f00,d7
	lsr #$04,d7
	lsr #$04,d7
	add.w #$30,d7
	move.w d7,(a4)
	move.w regy,d7
	andi.w #$00f0,d7
	lsr #$04,d7
	add.w #$30,d7
	move.w d7,(a4)	
	move.w regy,d7
	andi.w #$000f,d7
	add.w #$30,d7
	move.w d7,(a4)	
	rts	
	
renderpc:
	move.l #$0000258e,d0 ;reg 3 display VRAM position
	bsr calc_vram
	move.l d0,(a3)
	clr d7
	swap d7
	clr d7
	move.w regpc,d7
	andi.w #$F000,d7
	lsr #$06,d7
	lsr #$06,d7
	add.w #$30,d7
	move.w d7,(a4)
	move.w regpc,d7
	andi.w #$0f00,d7
	lsr #$04,d7
	lsr #$04,d7
	add.w #$30,d7
	move.w d7,(a4)
	move.w regpc,d7
	andi.w #$00f0,d7
	lsr #$04,d7
	add.w #$30,d7
	move.w d7,(a4)	
	move.w regpc,d7
	andi.w #$000f,d7
	add.w #$30,d7
	move.w d7,(a4)	
	rts

rendersp:
	move.l #$0000268e,d0 ;reg 3 display VRAM position
	bsr calc_vram
	move.l d0,(a3)
	clr d7
	swap d7
	clr d7
	move.w regsp,d7
	andi.w #$F000,d7
	lsr #$06,d7
	lsr #$06,d7
	add.w #$30,d7
	move.w d7,(a4)
	move.w regsp,d7
	andi.w #$0f00,d7
	lsr #$04,d7
	lsr #$04,d7
	add.w #$30,d7
	move.w d7,(a4)
	move.w regsp,d7
	andi.w #$00f0,d7
	lsr #$04,d7
	add.w #$30,d7
	move.w d7,(a4)	
	move.w regsp,d7
	andi.w #$000f,d7
	add.w #$30,d7
	move.w d7,(a4)	
	rts	
	
renderbyte:
	move.l #$00002792,d0 ;reg a display VRAM position
	bsr calc_vram
	move.l d0,(a3)
	clr d7
	swap d7
	clr d7
	move.b currentbyte,d7
	andi.b #$F0,d7
	lsr.w #$04,d7
	add.w #$30,d7
	move.w d7,(a4)	
	clr d7
	swap d7
	clr d7
	move.b currentbyte,d7
	andi.b #$0F,d7
	add.w #$30,d7
	move.w d7,(a4)		
	rts	
	
renderrealPC:
	move.l #$00002896,d0 ;reg 3 display VRAM position
	bsr calc_vram
	move.l d0,(a3)
	clr d7
	swap d7
	clr d7
	move.w emuoffset,d7
	andi.w #$F000,d7
	lsr #$06,d7
	lsr #$06,d7
	add.w #$30,d7
	move.w d7,(a4)
	move.w emuoffset,d7
	andi.w #$0f00,d7
	lsr #$04,d7
	lsr #$04,d7
	add.w #$30,d7
	move.w d7,(a4)
	move.w emuoffset,d7
	andi.w #$00f0,d7
	lsr #$04,d7
	add.w #$30,d7
	move.w d7,(a4)	
	move.w emuoffset,d7
	andi.w #$000f,d7
	add.w #$30,d7
	move.w d7,(a4)	
	rts		
	
renderlastbyte:
	move.l #$00002998,d0 ;reg a display VRAM position
	bsr calc_vram
	move.l d0,(a3)
	clr d7
	swap d7
	clr d7
	move.b lastbyte,d7
	andi.b #$F0,d7
	lsr.w #$04,d7
	add.w #$30,d7
	move.w d7,(a4)	
	clr d7
	swap d7
	clr d7
	move.b lastbyte,d7
	andi.b #$0F,d7
	add.w #$30,d7
	move.w d7,(a4)		
	rts		