instruction_table:
	dc.w hex00
	dc.w hex01
	dc.w hex02
	dc.w hex03
	dc.w hex04
	dc.w hex05
	dc.w hex06
	dc.w hex07
	dc.w hex08
	dc.w hex09 ;eor acc immediate
	dc.w hex0a ;ASL accumulator
	dc.w hex0b
	dc.w hex0c
	dc.w hex0d
	dc.w hex0e
	dc.w hex0f
	dc.w hex10
	dc.w hex11
	dc.w hex12
	dc.w hex13
	dc.w hex14
	dc.w hex15
	dc.w hex16
	dc.w hex17
	dc.w hex18
	dc.w hex19
	dc.w hex1a
	dc.w hex1b
	dc.w hex1c
	dc.w hex1d
	dc.w hex1e
	dc.w hex1f
	dc.w hex20
	dc.w hex21
	dc.w hex22
	dc.w hex23
	dc.w hex24
	dc.w hex25
	dc.w hex26
	dc.w hex27
	dc.w hex28
	dc.w hex29 ;and accumulator
	dc.w hex2a
	dc.w hex2b
	dc.w hex2c
	dc.w hex2d
	dc.w hex2e
	dc.w hex2f
	dc.w hex30
	dc.w hex31
	dc.w hex32
	dc.w hex33
	dc.w hex34
	dc.w hex35
	dc.w hex36
	dc.w hex37
	dc.w hex38
	dc.w hex39
	dc.w hex3a
	dc.w hex3b
	dc.w hex3c
	dc.w hex3d
	dc.w hex3e
	dc.w hex3f
	dc.w hex40
	dc.w hex41
	dc.w hex42
	dc.w hex43
	dc.w hex44
	dc.w hex45
	dc.w hex46
	dc.w hex47
	dc.w hex48
	dc.w hex49 ;eor acc
	dc.w hex4a ;lsr accumulator
	dc.w hex4b
	dc.w hex4c ;absolute jump
	dc.w hex4d
	dc.w hex4e
	dc.w hex4f
	dc.w hex50
	dc.w hex51
	dc.w hex52
	dc.w hex53
	dc.w hex54
	dc.w hex55
	dc.w hex56
	dc.w hex57
	dc.w hex58
	dc.w hex59
	dc.w hex5a
	dc.w hex5b
	dc.w hex5c
	dc.w hex5d
	dc.w hex5e
	dc.w hex5f
	dc.w hex60
	dc.w hex61
	dc.w hex62
	dc.w hex63
	dc.w hex64
	dc.w hex65
	dc.w hex66
	dc.w hex67
	dc.w hex68
	dc.w hex69 ;ADC immidiate
	dc.w hex6a ;ror accumulator
	dc.w hex6b
	dc.w hex6c
	dc.w hex6d
	dc.w hex6e
	dc.w hex6f
	dc.w hex70
	dc.w hex71
	dc.w hex72
	dc.w hex73
	dc.w hex74
	dc.w hex75
	dc.w hex76
	dc.w hex77
	dc.w hex78
	dc.w hex79
	dc.w hex7a
	dc.w hex7b
	dc.w hex7c
	dc.w hex7d
	dc.w hex7e
	dc.w hex7f
	dc.w hex80
	dc.w hex81
	dc.w hex82
	dc.w hex83
	dc.w hex84
	dc.w hex85
	dc.w hex86
	dc.w hex87
	dc.w hex88
	dc.w hex89
	dc.w hex8a
	dc.w hex8b
	dc.w hex8c
	dc.w hex8d
	dc.w hex8e
	dc.w hex8f
	dc.w hex90
	dc.w hex91
	dc.w hex92
	dc.w hex93
	dc.w hex94
	dc.w hex95
	dc.w hex96
	dc.w hex97
	dc.w hex98
	dc.w hex99
	dc.w hex9a
	dc.w hex9b
	dc.w hex9c
	dc.w hex9d
	dc.w hex9e
	dc.w hex9f
	dc.w hexa0 ;LDY immediate
	dc.w hexa1
	dc.w hexa2 ;LDX immediate
	dc.w hexa3
	dc.w hexa4
	dc.w hexa5
	dc.w hexa6
	dc.w hexa7
	dc.w hexa8
	dc.w hexa9 ;LDA immediate
	dc.w hexaa
	dc.w hexab
	dc.w hexac ;lDY absolute word
	dc.w hexad
	dc.w hexae ;lDX absolute word
	dc.w hexaf
	dc.w hexb0
	dc.w hexb1
	dc.w hexb2
	dc.w hexb3
	dc.w hexb4
	dc.w hexb5
	dc.w hexb6
	dc.w hexb7
	dc.w hexb8
	dc.w hexb9
	dc.w hexba
	dc.w hexbb
	dc.w hexbc
	dc.w hexbd
	dc.w hexbe
	dc.w hexbf
	dc.w hexc0
	dc.w hexc1
	dc.w hexc2
	dc.w hexc3
	dc.w hexc4
	dc.w hexc5
	dc.w hexc6
	dc.w hexc7
	dc.w hexc8
	dc.w hexc9
	dc.w hexca
	dc.w hexcb
	dc.w hexcc
	dc.w hexcd
	dc.w hexce
	dc.w hexcf
	dc.w hexd0
	dc.w hexd1
	dc.w hexd2
	dc.w hexd3
	dc.w hexd4
	dc.w hexd5
	dc.w hexd6
	dc.w hexd7
	dc.w hexd8
	dc.w hexd9
	dc.w hexda
	dc.w hexdb
	dc.w hexdc
	dc.w hexdd
	dc.w hexde
	dc.w hexdf
	dc.w hexe0
	dc.w hexe1
	dc.w hexe2
	dc.w hexe3
	dc.w hexe4
	dc.w hexe5
	dc.w hexe6
	dc.w hexe7
	dc.w hexe8
	dc.w hexe9 ;SBC immediate
	dc.w hexea ;nop
	dc.w hexeb
	dc.w hexec
	dc.w hexed
	dc.w hexee
	dc.w hexef
	dc.w hexf0
	dc.w hexf1
	dc.w hexf2
	dc.w hexf3
	dc.w hexf4
	dc.w hexf5
	dc.w hexf6
	dc.w hexf7
	dc.w hexf8
	dc.w hexf9
	dc.w hexfa
	dc.w hexfb
	dc.w hexfc
	dc.w hexfd
	dc.w hexfe
	dc.w hexff
hex00:
	rts
hex01:
hex02:
hex03:
hex04:
hex05:
hex06:
hex07:
hex08:
hex09:
	move.b (a6)+,d2	
	clr d5
	move.b d2,currentbyte
	add.w #$0001,regPC	
	move.b rega,d5
	or.b d5,d2
	move.b d2,rega
	rts
hex0a:
	move.b (a6)+,d2	
	move.b d2,currentbyte
	move.b rega,d2
    asl.b #$01,d2
	move.b d2,rega
	add.w #$0001,regPC	
	rts
hex0b:
hex0c:
hex0d:
hex0e:
hex0f:
hex10:
hex11:
hex12:
hex13:
hex14:
hex15:
hex16:
hex17:
hex18:
hex19:
hex1a:
hex1b:
hex1c:
hex1d:
hex1e:
hex1f:
hex20:
hex21:
hex22:
hex23:
hex24:
hex25:
hex26:
hex27:
hex28:
hex29:
	move.b (a6)+,d2	
	clr d5
	move.b d2,currentbyte
	add.w #$0001,regPC	
	move.b rega,d5
	and.b d5,d2
	move.b d2,rega
	rts
hex2a:
hex2b:
hex2c:
hex2d:
hex2e:
hex2f:
hex30:
hex31:
hex32:
hex33:
hex34:
hex35:
hex36:
hex37:
hex38:
hex39:
hex3a:
hex3b:
hex3c:
hex3d:
hex3e:
hex3f:
hex40:
hex41:
hex42:
hex43:
hex44:
hex45:
hex46:
hex47:
hex48:
hex49:
	move.b (a6)+,d2	
	clr d5
	move.b d2,currentbyte
	add.w #$0001,regPC	
	move.b rega,d5
	eor.b d5,d2
	move.b d2,rega
	rts
hex4a:
	move.b (a6)+,d2	
	move.b d2,currentbyte
	move.b rega,d2
    lsr.b #$01,d2
	move.b d2,rega
	add.w #$0001,regPC	
	rts
hex4b:
hex4c:
	move.b (a6)+,d2	
	move.b d2,currentbyte	
	add.w #$0001,regPC	
	sub.w d2,a6
	sub.w d2,a6
	sub.w d2,regpc
	sub.w d2,regpc
	rts
hex4d:
hex4e:
hex4f:
hex50:
hex51:
hex52:
hex53:
hex54:
hex55:
hex56:
hex57:
hex58:
hex59:
hex5a:
hex5b:
hex5c:
hex5d:
hex5e:
hex5f:
hex60:
hex61:
hex62:
hex63:
hex64:
hex65:
hex66:
hex67:
hex68:
hex69:
	move.b (a6)+,d2
	move.b d2,currentbyte
	add.b d2,regA
	add.w #$0001,regPC
	rts
hex6a:
	move.b (a6)+,d2	
	move.b d2,currentbyte
	move.b rega,d2
    ror.b #$01,d2
	move.b d2,rega
	add.w #$0001,regPC	
	rts
hex6b:
hex6c:
hex6d:
hex6e:
hex6f:
hex70:
hex71:
hex72:
hex73:
hex74:
hex75:
hex76:
hex77:
hex78:
hex79:
hex7a:
hex7b:
hex7c:
hex7d:
hex7e:
hex7f:
hex80:
hex81:
hex82:
hex83:
hex84:
hex85:
hex86:
hex87:
hex88:
hex89:
hex8a:
hex8b:
hex8c:
hex8d:
hex8e:
hex8f:
hex90:
hex91:
hex92:
hex93:
hex94:
hex95:
hex96:
hex97:
hex98:
hex99:
hex9a:
hex9b:
hex9c:
hex9d:
hex9e:
hex9f:
hexa0:
	move.b (a6)+,d2
	move.b d2,currentbyte	
	add.w #$0001,regPC	
	move.w d2, regy
	rts
hexa1:
hexa2:
	move.b (a6)+,d2
	move.b d2,currentbyte	
	add.w #$0001,regPC	
	move.w d2, regX
	rts
hexa3:
hexa4:
hexa5:
hexa6:
hexa7:
hexa8:
hexa9:
	move.b (a6)+,d2
	move.b d2,currentbyte	
	add.w #$0001,regPC	
	move.b d2, regA
	rts
hexaa:
hexab:
hexac:
	clr d5
	clr d2
	move.b (a6)+,d2
	move.b (a6)+,d5			
	lsl.w #$08,d5    ;switch from little to big endian
	eor.w d5,d2      ;ditto
	move.w d2,regy
	add.w #$0003,regPC
	rts
hexad:
hexae:
	clr d5
	clr d2
	move.b (a6)+,d2
	move.b (a6)+,d5			
	lsl.w #$08,d5    ;switch from little to big endian
	eor.w d5,d2      ;ditto
	move.w d2,regx
	add.w #$0003,regPC
	rts
hexaf:
hexb0:
hexb1:
hexb2:
hexb3:
hexb4:
hexb5:
hexb6:
hexb7:
hexb8:
hexb9:
hexba:
hexbb:
hexbc:
hexbd:
hexbe:
hexbf:
hexc0:
hexc1:
hexc2:
hexc3:
hexc4:
hexc5:
hexc6:
hexc7:
hexc8:
hexc9:
hexca:
hexcb:
hexcc:
hexcd:
hexce:
hexcf:
hexd0:
hexd1:
hexd2:
hexd3:
hexd4:
hexd5:
hexd6:
hexd7:
hexd8:
hexd9:
hexda:
hexdb:
hexdc:
hexdd:
hexde:
hexdf:
hexe0:
hexe1:
hexe2:
hexe3:
hexe4:
hexe5:
hexe6:
hexe7:
hexe8:
hexe9:
	move.b (a6)+,d2
	move.b d2,currentbyte
	sub.b d2,regA
	add.w #$0001,regPC
	rts
hexea: ;nop
	move.b (a6)+,d2	
	move.b d2,currentbyte	
	add.w #$0001,regPC	
	rts
hexeb:
hexec:
hexed:
hexee:
hexef:
hexf0:
hexf1:
hexf2:
hexf3:
hexf4:
hexf5:
hexf6:
hexf7:
hexf8:
hexf9:
hexfa:
hexfb:
hexfc:
hexfd:
hexfe:
hexff:		
	rts	