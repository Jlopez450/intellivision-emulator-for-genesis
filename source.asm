    include "ramdat.asm"
		      dc.l $FFFF00,   start,     ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
	          dc.l HBlank,    ErrorTrap, VBlank,    ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
      	      dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.l ErrorTrap, ErrorTrap, ErrorTrap, ErrorTrap
              dc.b "SEGA GENESIS    "
              dc.b "(C)  COPYRIGHT  "
              dc.b "PROGRAM NAME HERE                               "
              dc.b "PROGRAM NAME HERE                               "
              dc.b "GM 01234567-89"
              dc.w $DEAD        ;checksum
              dc.b "J               "
              dc.l 0
              dc.l ROM_End
              dc.l $FF0000
              dc.l $FFFFFF
              dc.b "    "
              dc.b "    "
              dc.b "    "
              dc.b "            "                           
              dc.b "This program contains blast processing! "
              dc.b "JUE             "
start:                          
        move.b  $A10001,d0
        andi.b  #$0F,d0
        beq.b   no_tmss       ;branch if oldest MD/GEN
        move.l  #'SEGA',$A14000 ;satisfy the TMSS        
no_tmss:
		move.w #$2700, sr       ;disable ints
        bsr setup_vdp
		bsr clear_vram
		lea ($fffe00),a6
		move.w #$0000,(a6)+
		move.w #$0000,(a6)+
		move.w #$0000,(a6)+
		move.w #$0000,(a6)+
		move.w #$0000,(a6)+
		move.w #$0000,(a6)+
		move.w #$0000,(a6)+
		move.w #$0000,(a6)+
		move.w #$0000,(a6)+
		move.w #$0000,(a6)+

		move.l #$40000000,(a3)
		lea (font),a5
		move.w #$0C00,d4
		bsr vram_loop
		
		lea (overlay),a5
		move.l #$60000000,(a3)
		bsr termtextloop
		
		move.l #$c0000000,(a3)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0000,(a4)
		move.w #$0eee,(a4)
		lea (program),a6
        move.w #$2300, sr       ;enable ints		
		
loop:
		bsr thing
		bra loop	
thing:
		cmpi.b #$df,d3 ;button c
		beq emu_core
		rts
		

setup_vdp:
        lea    $C00004.l,a3     ;VDP control port
	    lea    $C00000.l,a4     ;VDP data port
        lea    (VDPSetupArray).l,a5
        move.w #0018,d4         ;loop counter
VDP_Loop:
        move.w (a5)+,(a3)       ;load setup array
	    nop
        dbf d4,VDP_Loop
        rts  
vram_Loop:
        move.w (a5)+,(a4)       ;load setup array
        dbf d4,vram_Loop
        rts 
clear_vram:		       
        move.l  #$40000000,(a3) ;set VRAM write $0000
clear_loop:             
        move.w  #$0000,(a4)
        dbf d4,clear_loop
        rts		
termtextloop:					;for strings with terminators
		move.b (a5)+,d4	
		cmpi.b #$26,d4			;& end of text flag)
		beq return
		andi.w #$00ff,d4
		add.w #$0040,d4
        move.w d4,(a4)		
		bra termtextloop
calc_vram:
		move.l d1,-(sp)
		move.l d0,d1
		andi.w #$C000,d1 ;get first two bits only
		lsr.w #$7,d1     ;shift 14 spaces to move it to the end
		lsr.w #$7,d1     ;ditto
		andi.w #$3FFF,d0 ;clear all but first two bits
		eor.w #$4000,d0  ;attach vram write bit
		swap d0          ;move d0 to high word
		eor.w d1,d0      ;smash the two halves together	
		move.l (sp)+,d1
		rts	
read_controller:                ;d3 will have final controller reading!
		moveq	#0, d3            
	    moveq	#0, d7
	    move.b  #$40, ($A10009) ;Set direction
	    move.b  #$40, ($A10003) ;TH = 1
    	nop
	    nop
	    move.b  ($A10003), d3	;d3.b = X | 1 | C | B | R | L | D | U |
	    andi.b	#$3F, d3		;d3.b = 0 | 0 | C | B | R | L | D | U |
	    move.b	#$0, ($A10003)  ;TH = 0
	    nop
	    nop
	    move.b	($A10003), d7	;d7.b = X | 0 | S | A | 0 | 0 | D | U |
	    andi.b	#$30, d7		;d7.b = 0 | 0 | S | A | 0 | 0 | 0 | 0 |
	    lsl.b	#$2, d7		    ;d7.b = S | A | 0 | 0 | D | U | 0 | 0 |
	    or.b	d7, d3			;d3.b = S | A | C | B | R | L | D | U |
		rts	
unhold:
		bsr read_controller
		cmpi.b #$ff,d3
		 beq return
		bra unhold		
return:
		rts
returnint:
		rte
		
ErrorTrap:        
        bra ErrorTrap

HBlank:
        rte

VBlank:
		bsr read_controller
		cmpi.b #$bf,d3 ;button A
		beq singlestep
	    ;bsr emu_core
		bsr render
        rte
		
	include "renderer.asm"	

singlestep:
		bsr unhold
hold:
		bsr emu_core
		rte
emu_core:
		moveq.l #$00000000,d2
		move.w (a6)+,d2
	    move.w d2,lastbyte		
		add.w #$0002,regPC	
		lea (instruction_table),a0
        lsl.w #$1,d2		    ;locate the correct table
		;sub.w #$02,d2          ;step back a word 
		add.w d2,a0             ;adjust the address register 
		move.w (a0),d2
		move.l d2,a0            ;switch to direct addressing
		move.w d2,emuoffset
		clr d2
		jmp (a0)
		
		;move.w instruction_table(pc,d2.b)

	include "instruction_table.asm"	;big ass jump table
		
VDPSetupArray:
	dc.w $8014		
	dc.w $8174  ; Genesis mode, DMA enabled, VBLANK-INT enabled		
	dc.w $8208	;field A    
	dc.w $8300	;$833e	
	dc.w $8401	;field B	
	dc.w $8518	;sprite
	dc.w $8600
	dc.w $8700  ;BG color		
	dc.w $8800
	dc.w $8900
	dc.w $8AFF		
	dc.w $8B00		
	dc.w $8C81	
	dc.w $8D34		
	dc.w $8E00
	dc.w $8F02	;auto increment	
	dc.w $9001		
	dc.w $9100		
	dc.w $9200

overlay:
	dc.b "                                                                "
	dc.b "     ComradeOj's CP-1610 simulator                              "
	dc.b "                                                                "
	dc.b " Press 'A' to single step                                       "	
	dc.b "---------------Registers----------------------------------------"	
	dc.b "  R0: $????                                                     ";228e
	dc.b "  R1: $????                                                     "
	dc.b "  R2: $????                                                     "
	dc.b "  R3: $????                                                     "
	dc.b "  R4: $????                                                     "
	dc.b "  R5: $????                                                     "
	dc.b "                                                                "
	dc.b "  SP: $????                                                     "
	dc.b "  PC: $????                                                     "
	dc.b "                                                                "
	dc.b "  Flags:$????                                                   "	
	dc.b "                                                                "
	dc.b "  Current word:$                                                "
	dc.b "     Last word:$                                                "
	dc.b "                                                                "
	dc.b "                                                               &"
	
program:	
	incbin "exec.int"
	
font:	
	incbin "hexascii.bin"
	incbin "ascii.bin"

ROM_End:
              
              